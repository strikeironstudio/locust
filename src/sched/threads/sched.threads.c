#ifndef THREADS_H
#include <locust/sched.threads.h>
#endif

TYPE_ID_DEF(thread);

/*
typedef struct thread {
	uint64 id;
	task *curr_task;
	context *sched_ctx;
	struct thread* thief;
	struct thread* victim;
	deque tasks;
} thread;
*/
META_FIELDS_DEF(thread) = {
	5, {
		META_FIELD_DEF(thread, id, uint64),
		META_FIELD_DEF(thread, curr_task, ptr), // task*
		META_FIELD_DEF(thread, sched_ctx, ptr), // context*
		META_FIELD_DEF(thread, thief, ptr), // thread*
		META_FIELD_DEF(thread, victim, ptr), // thread*
		META_FIELD_DEF(thread, tasks, ptr), // deque*
    }
};

META_DEF(thread, & META_FIELDS_OF(thread), nil);

#ifndef MEM_H
#include <locust/rt.mem.h>
#endif

#ifndef INSPECT_H
#include <locust/rt.inspect.h>
#endif

#ifndef WAITS_H
#include <locust/sched.waits.h>
#endif

#ifndef DEBUG_H
#include <locust/rt.debug.h>
#endif

#ifndef MAP_H
#include <locust/ds.map.h>
#endif

#include <stdio.h>
#include <errno.h>
#include <tinycthread.h>

bool threads_ready = false;
bool threads_go = false;
thread* threads;

uint64 threads_count = 0;
uint64 threads_id_counter = 0;

thrd_t * threads_internal_id;

__thread thread thread_curr;

static int thread_scheduler(ptr arg);

void threads_init(uint64 thread_count) {
	if (thread_count == 0) { thread_count = 1; }
	threads_count = thread_count;

	threads = mem_get(sizeof(thread)*thread_count);
	error_if(threads == nil, "Error initializing memory for threads.");
    mem_set(threads, sizeof(thread)*thread_count, 0);

	threads_internal_id = mem_get(sizeof(thrd_t)*thread_count);
	error_if(threads_internal_id == nil, "Error initializing memory for threads_internal_id.");

	uint64 curr_id = 0;
	uint64 thief_id = thread_count-1;
	uint64 victim_id = 1;
	thread* t = threads;

	for (uint64 thread_counter=0; thread_counter < thread_count;) {
		// Make sure we know about adjacent threads, and that they know about us.
		// By the time every thread does this, everyone will know their neighbors.

		t->thief = threads+thief_id;
		(threads+thief_id)->victim = t;

		t->victim = threads+victim_id;
		(threads+victim_id)->thief = t;

		deque_init(&t->tasks, & META_OF(ptr));

		++thread_counter;
		++t;
		++curr_id;
		thief_id = curr_id - 1;
		victim_id = (curr_id + 1) % threads_count;
	}

	threads_ready = true;
    return;
}

void threads_deinit(void) {
	mem_free(threads);
	mem_free(threads_internal_id);
	threads = nil;
	threads_count = 0;
	return;
}

void threads_launch(void) {
	assert(threads_ready);
	debug("launching %ld threads\n", threads_count);

	// Launch threads
	int err;
	for (uint64 i=1; i < threads_count; ++i) {
        err = thrd_create(threads_internal_id+i, &thread_scheduler, (void*)i);
        if (err != thrd_success) {
            error("Error spawning thread %lu.\n", i);
            switch(err) {
                case thrd_nomem:
                    die("thrd_nomem: Not enough memory.\n");
                case thrd_error:
                    die("thrd_error: Request could not be honored.\n");
                default:
                    die("thrd_?????: Unknown error.\n");
            }
        }
    }
	debug("launching main thread\n");
	// Now launch as main thread
	thread_scheduler(0);
}

static int thread_scheduler(ptr arg) {
	uint64 curr_id = (uint64)arg;
	uint64 thief_id = (curr_id == 0) ? threads_count : curr_id-1;
	uint64 victim_id = (curr_id + 1) % threads_count;
	thread* t = threads+curr_id;
	t->id = curr_id;
	thread_curr = *t;

	t->curr_task = nil;
	t->sched_ctx = nil; // This is set up later, inside the thread scheduler.


    // Inspired by code from http://www.1024cores.net/home/scalable-architecture/task-scheduling-strategies
	debug("inside thread %lu\n", curr_id);

	task *tsk;
	context ctx;
	context *old_ctx;
	context *tsk_ctx;
	t->sched_ctx = &ctx;
    uint64 backoff_rate = 0;

    // get ready, get set, go!
    if (curr_id == 0) {
    	threads_go = true;
    } else {
    	while (threads_go == false) {
    		wait_gradual(backoff_rate++);
    	}
    	backoff_rate = 0;
    }

	while (true) {
		debug("thread %lu getting task\n", curr_id);
		if (deque_front_pop(&t->tasks, & META_OF(ptr), &tsk) ||
			deque_front_pop(&t->victim->tasks, & META_OF(ptr), &tsk)) {
		    //repr(&tsk, & META_OF(task));
			debug("\nthread %lu got task: %ld named \"%s\" -- %ld and %ld tasks in deques\n", t->id, tsk->id, tsk->name, t->tasks.len, t->victim->tasks.len);
			task_set_ready_off(tsk);
			t->curr_task = tsk;
			debug("thread %lu context_swap\n", t->id);

			tsk_ctx = &tsk->ctx;
			old_ctx = &ctx;

			context_swap(old_ctx, tsk_ctx);
			debug("thread %lu context_swap done!\n", t->id);

			t->curr_task = nil;
            backoff_rate = 0;
        } else {
			debug("thread %lu waiting\n", t->id);
            wait_gradual(backoff_rate++);
        }
	}
	return 0;
}


