#ifndef RING_H
#include <locust/ds.ring.h>
#endif

#ifndef MEM_H
#include <locust/rt.mem.h>
#endif

#ifndef ATOMIC_H
#include <locust/sched.atomic.h>
#endif

#ifndef WAITS_H
#include <locust/sched.waits.h>
#endif

#ifndef DEBUG_H
#include <locust/rt.debug.h>
#endif

bool
static inline nonnullfunc
ring_write_single(ring restrict self, ptr restrict item) {
    uint64 front, back, index;
    size_t item_size = ring_type(self)->size;

    front = *ring_wa_counter(self); // These could increment after this but only the
    back = *ring_r_counter(self);  // a_counter is bad to have increment until the cas

    // Check for butting up against the back from other side.
    // If so, not necessarily full at this moment in time, but close enough.
    if (front - back == ring_len(self)) { return false; }

    index = ((*ring_wa_counter(self))++) & (ring_len(self)-1);
    
    // Guaranteed to have acquired a spot in the ring. Shunt item in.
    mem_copy(item, ring_index_unsafe(self, index*item_size), item_size);
    
    // Update the other side once we've written.
    ++(*ring_w_counter(self));
    return true;
}

bool
static inline nonnullfunc
ring_write_multi(ring restrict self, ptr restrict item) {
    uint64 front, back, index;
    uint8 wait_counter = 0;
    size_t item_size = ring_type(self)->size;

    // This loop either adds the item, or hits a full ring buffer.
    while (true) {
        front = *ring_wa_counter(self); // These could increment after this but only the
        back = *ring_r_counter(self);  // a_counter is bad to have increment until the cas

        // Check for butting up against the back from other side.
        // If so, not necessarily full at this moment in time, but close enough.
        if (front - back == ring_len(self)) { return false; }

        // Attempt to acquire a spot in the ring.
        // If not, start this whole attempt over with new spot to acquire.
        if (!atomic_cmp_and_set(ring_wa_counter(self), front, front+1)) { continue; }

        // Guaranteed to have acquired a spot in the ring. Shunt item in.
        index = front & (ring_len(self)-1);
        mem_copy(item, ring_index_unsafe(self, index*item_size), item_size);

        // This increases ring_w_counter only if we're responsible for it.
        while (!atomic_cmp_and_set(ring_w_counter(self), front, front+1)) {
            wait_gradual(wait_counter++);
        }

        return true;
    }
}

bool
nonnullfunc
ring_write(ring restrict self, ptr restrict item) {
    // determine usage of ring by number of threads using it.
    uint64* w_thread_id = ring_w_thread_id(self);
 
    switch (*w_thread_id) {
        case -1ULL: // first usage by any thread. "mark" ring as written by current thread.
            *w_thread_id = thread_curr.id;
        default: // known to be a single writer so far, check again.
            if (*w_thread_id != thread_curr.id) {
                // oh shit first usage with multiple writers
                *w_thread_id = -2ULL;
        case -2ULL: // known to be multiple writers
                return ring_write_multi(self, item);
            }
            // known to be single writer
            return ring_write_single(self, item);
    }
}
