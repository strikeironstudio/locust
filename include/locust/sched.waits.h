#define WAITS_H

#ifndef TYPES_H
#include <locust/type.types.h>
#endif

void static inline wait_active(uint8 rate) {
    do {
        __asm__("pause");
    } while (rate-->0);
}

void wait_gradual(uint8 rate);

