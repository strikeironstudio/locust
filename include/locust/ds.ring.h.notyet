#define RING_H

#ifndef TYPES_H
#include <locust/type.types.h>
#endif

#ifndef DEBUG_H
#include <locust/rt.debug.h>
#endif

#ifndef MEM_H
#include <locust/rt.mem.h>
#endif

#ifndef ARRAY_H
#include <locust/ds.array.h>
#endif

#ifndef WAITS_H
#include <locust/sched.waits.h>
#endif

#define ring__type_id ((type_id)30)
extern TYPE_ID_DECL(ring);
extern META_DECL(ring);

typedef ptr ring;

#define CACHE_LINE_SIZE 64

uint64
static inline purestfunc
round_up_to_pow2(uint64 x) {
    --x;
    x |= x >> 1;
    x |= x >> 2;
    x |= x >> 4;
    x |= x >> 8;
    x |= x >> 16;
    x |= x >> 32;
    return ++x;
}

// rings look like
// typedef struct {
//     // cache line 1
//     uint64 r_counter;  // read
//     uint64 ra_counter; // read acquire -- readers use to claim space. they update r_counter when done.
//     uint64 r_thread_id;// used to ensure XPSC OR XPMC usage
//     byte __pad1[CACHE_LINE_SIZE - 3*sizeof(uint64)];
//
//     // cache line 2
//     uint64 w_counter;  // write
//     uint64 wa_counter; // write acquire -- writers use to claim space. they update w_counter when done.
//     uint64 w_thread_id;// used to ensure SPXC or MPXC usage
//     byte __pad2[CACHE_LINE_SIZE - 3*sizeof(uint64)];
//
//     // cache line 3
//     array items;
// } ring;
// where n is the length of the ring items in bytes
// and a ring is a ptr whose value is &items[0],
// which front's value also is.
// So, length and meta data are behind the ptr

ptr
static inline nonnullfunc purestfunc
ring_mem_front(ring self) {
    return array_mem_front(self) - (2*CACHE_LINE_SIZE);
}

uint64
static inline nonnullfunc purestfunc
ring_mem_len(size_t type_size, uint64 count) {
    // ring needs to be a power of 2, round up to make it so.
    count = round_up_to_pow2(count);
    count = type_size * count;
    count += (2*sizeof(ptr)) + sizeof(uint64) + (2*CACHE_LINE_SIZE);
    return count;
}

static inline nonnullfunc meta const * ring_type(ring self) { return array_type(self); }
uint64 static inline nonnullfunc ring_len(ring self) { return array_len(self); }
ptr static inline overloaded nonnullfunc ring_index(ring self, uint64 i) { return array_index(self, i); }
ptr static inline overloaded nonnullfunc ring_index(ring self, int64 i) { return array_index(self, i); }
ptr static inline overloaded nonnullfunc ring_index_unsafe(ring self, uint64 i) { return array_index_unsafe(self, i); }
ptr static inline overloaded nonnullfunc ring_index_unsafe(ring self, int64 i) { return array_index_unsafe(self, i); }
static inline nonnullfunc purestfunc uint64* ring_r_counter(ring self) { return ring_mem_front(self); }
static inline nonnullfunc purestfunc uint64* ring_ra_counter(ring self) { return ring_mem_front(self) + sizeof(uint64); }
static inline nonnullfunc purestfunc uint64* ring_r_thread_id(ring self) { return ring_mem_front(self); + 2*sizeof(uint64); }
static inline nonnullfunc purestfunc uint64* ring_w_counter(ring self) { return ring_mem_front(self) + CACHE_LINE_SIZE; }
static inline nonnullfunc purestfunc uint64* ring_wa_counter(ring self) { return ring_mem_front(self) + CACHE_LINE_SIZE + sizeof(uint64); }
static inline nonnullfunc purestfunc uint64* ring_w_thread_id(ring self) { return ring_mem_front(self); + CACHE_LINE_SIZE + 2*sizeof(uint64); }

bool nonnullfunc ring_write(ring restrict self, ptr restrict item);
bool nonnullfunc ring_read(ring restrict self, ptr restrict item);

ring
static inline nonnullfunc
ring_init(ring restrict self, meta const * const restrict type, uint64 count) {
    *((uint64*)self) = 0;
    *((uint64*)(self+sizeof(uint64))) = 0;
    *((uint64*)(self+2*sizeof(uint64))) = -1ULL;
    self += CACHE_LINE_SIZE;
    *((uint64*)self) = 0;
    *((uint64*)(self+sizeof(uint64))) = 0;
    *((uint64*)(self+2*sizeof(uint64))) = -1ULL;
    self += CACHE_LINE_SIZE;
    self = array_init(self, type, count);
    return self;
}

ring
static inline nonnullfunc
ring_create(meta const * const type, uint64 count) {
    ring ret = mem_get(ring_mem_len(type->size, count), & META_OF(ring));
    error_goto_if(ret == nil, "NilPtr: ret; mem_get() failure");
    return ring_init(ret, type, count);
  error:
    return nil;
}

void 
static inline nonnullfunc
ring_destroy(ring self) {
    mem_free(self - (CACHE_LINE_SIZE*2 + sizeof(meta const *) + sizeof(uint64)));
}
