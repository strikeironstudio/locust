#define TEST_H
#undef NDEBUG

#include <stdio.h>
#include <stdlib.h>

#ifndef TYPES_H
#include <type.types.h>
#endif

#ifndef DEBUG_H
#include <rt.debug.h>
#endif

// #define test_run(test) info("\n-----%s", " " #test); \
//     if (test()) { info("PASS"); ++tests_passed; } \
//     else { error("FAIL"); ++tests_failed; } \
//     ++tests_ran;

// #define tests_start(name) int main(int argc, char *argv[]) { \
//     argc = 1; \
//     info("----- TESTING: %s", argv[0]); \

// #define tests_end() info("Passed: %llu / %llu\nFailed: %llu / %llu\n", tests_passed, tests_ran, tests_failed, tests_ran); \
//     exit(tests_failed > 0); \
// }

// uint64 tests_failed = 0;
// uint64 tests_passed = 0;
// uint64 tests_ran = 0;
