#define DEBUG_H

extern __attribute__((__format__ (__printf__, 7, 8)))
int _log(char const * const mode,
	      char const * const func_name,
	      char const * const file_name,
		  int line,
		  int do_die,
          int do_info,
		  char const * const format,
		  ...);

#define die(...)   _log("DIE", __func__, __FILE__, __LINE__, 1, 1, __VA_ARGS__)
#define error(...) _log("ERR", __func__, __FILE__, __LINE__, 0, 1, __VA_ARGS__)
#define warn(...)  _log("WRN", __func__, __FILE__, __LINE__, 0, 1, __VA_ARGS__)
#define info(...)  _log("INF", __func__, __FILE__, __LINE__, 0, 1, __VA_ARGS__)
#define debug(...) _log("DBG", __func__, __FILE__, __LINE__, 0, 1, __VA_ARGS__)
#define log_quiet(...) _log("", "", "", -1, 0, 0, __VA_ARGS__)

#define die_if(EXPR, ...)   ((EXPR) &&   die(__VA_ARGS__))
#define error_if(EXPR, ...) ((EXPR) && error(__VA_ARGS__))
#define warn_if(EXPR, ...)  ((EXPR) &&  warn(__VA_ARGS__))
#define info_if(EXPR, ...)  ((EXPR) &&  info(__VA_ARGS__))
#define debug_if(EXPR, ...) do { if (EXPR) { debug(__VA_ARGS__); }} while(0)

#define error_goto(...) do { error(__VA_ARGS__); goto error; } while(0)
#define warn_goto(...)  do {  warn(__VA_ARGS__); goto warn;  } while(0)
#define info_goto(...)  do {  info(__VA_ARGS__); goto info;  } while(0)
#define debug_goto(...) do { debug(__VA_ARGS__); goto debug; } while(0)

#define error_goto_if(EXPR, ...) do { if (EXPR) { error(__VA_ARGS__); goto error; }} while(0)
#define warn_goto_if(EXPR, ...)  do { if (EXPR) {  warn(__VA_ARGS__); goto warn;  }} while(0)
#define info_goto_if(EXPR, ...)  do { if (EXPR) {  info(__VA_ARGS__); goto info;  }} while(0)
#define debug_goto_if(EXPR, ...) do { if (EXPR) { debug(__VA_ARGS__); goto debug; }} while(0)

#define __stringify(a) __stringify_(a)
#define __stringify_(a) #a

#define assert(EXPR) ((EXPR) ? (void)0 : (void) error(__stringify(EXPR)))
#define asserts(EXPR, MSG) _Static_assert(EXPR, "[ERR]: (" __FILE__ ":" __stringify(__LINE__) " in " __func__ ") " __stringify(MSG))

#ifdef NDEBUG
#define debug(...) ((void)0)
#define debug_if(EXPR, ...) ((void)0)
#define debug_goto_if(EXPR, ...) ((void)0)
#define debug_goto(...) ((void)0)
#endif


#define test_prep() char *message = NULL
#define test_assert(test, message) if (!(test)) { error(message); return message; }
#define test_run(test) log_quiet("---- %s", " " #test); message = test(); tests_run++; if (message) return message;
#define test_main(name) int main(int argc, char *argv[]) {\
    argc = 1; \
    log_quiet("---- TESTING: %s", argv[0]);\
    char *result = name();\
    if (result != 0) {\
        log_quiet("FAILED: %s", result);\
    }\
    else {\
        log_quiet("PASSED");\
    }\
    log_quiet("Tests run: %d", tests_run);\
    exit(result != 0);\
}

int tests_run;
