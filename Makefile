ROOT = $(dir $(CURDIR)/$(word $(words $(MAKEFILE_LIST)),$(MAKEFILE_LIST)))

BUILD_DIR = $(ROOT)build
TEST_DIR = $(ROOT)tests
STATS_DIR = $(ROOT)stats

INC_DIRS = -I$(CURDIR)/include
OPT_MODE = -O0 -g3 -DDEBUG

C_MODE = -std=gnu99 -D__TARGET_FREEBSD
WARNMODE = -W -Wall -Wextra -Winline -Wno-unused-function -Wno-unused-variable -Wno-unused-label -Wno-unused-parameter -Wno-unused-value -ferror-limit=5 -Wtype-limits -Wstrict-overflow=5 -fstrict-overflow -Wsign-compare
C_FLAGS = $(OPT_MODE) -fPIC -fomit-frame-pointer -c $(C_MODE) $(WARNMODE) -static -pthread $(INC_DIRS)
C_FILES = $(shell find $(ROOT)src/ -iname '*.c')
C_OBJ = $(subst $(ROOT), $(BUILD_DIR)/, $(C_FILES:.c=.c.o))

ASM_FLAGS = $(OPT_MODE) -c -integrated-as
ASM_FILES = $(shell find $(ROOT)src/ -iname '*.s')
ASM_OBJ = $(subst $(ROOT), $(BUILD_DIR)/, $(ASM_FILES:.s=.s.o))

AR = ar
AR_FLAGS = -r -s

TARGET = liblocust.a

TEST_C_FILES = $(shell find $(TEST_DIR) -iname '*.test.c')
TEST_C_OBJ = $(subst $(TEST_DIR)/, $(BUILD_DIR)/test/, $(TEST_C_FILES:.test.c=.test.exe))
TEST_C_FLAGS = $(OPT_MODE) -fPIC -fomit-frame-pointer $(C_MODE) $(WARNMODE) $(INC_DIRS) -L$(BUILD_DIR) -static -llocust -pthread

all: clean lib test stats debug
.PHONY : all

.PHONY : clean
clean:
	rm -rf $(BUILD_DIR)
	mkdir -p $(dir $(C_OBJ))
	mkdir -p $(dir $(ASM_OBJ))
	mkdir -p $(dir $(TEST_C_OBJ))

$(BUILD_DIR)/%.c.o: $(ROOT)%.c
	$(CC) $(C_FLAGS) -o $@ $<

$(BUILD_DIR)/%.s.o: $(ROOT)%.s
	$(CC) $(ASM_FLAGS) -o $@ $<

.PHONY : lib
lib: $(ASM_OBJ) $(C_OBJ)
	$(AR) $(AR_FLAGS) $(BUILD_DIR)/$(TARGET) $(ASM_OBJ) $(C_OBJ)

$(BUILD_DIR)/test/%.test.exe: $(TEST_DIR)/%.test.c
	$(CC) -o $@ $< $(TEST_C_FLAGS)

.PHONY : test
test: $(TEST_C_OBJ)
	for x in $(TEST_C_OBJ); do $$x; done

.PHONY : memcheck
memcheck:
	valgrind --read-var-info=yes --leak-check=yes --leak-check=full --show-leak-kinds=all --track-origins=yes > $(STATS_DIR)/stats.memcheck.txt 2>&1

.PHONY : helgrind
helgrind:
	valgrind --tool=helgrind --read-var-info=yes > $(STATS_DIR)/stats.helgrind.txt 2>&1

.PHONY : drd
drd:
	valgrind --tool=drd --read-var-info=yes > $(STATS_DIR)/stats.drd.txt 2>&1

.PHONE : ebnf
ebnf:
	python3.2 -m compiler.parser > $(STATS_DIR)/stats.ebnf.txt 2>&1

.PHONE : cloc
cloc:
	cloc --exclude-dir=build --exclude-dir=.git ./ > $(STATS_DIR)/stats.cloc.txt 2>&1

.PHONY : benchmark
benchmark:
	$(CC) $(C_MODE) -Oz -g3 -DDEBUG -static -I/usr/include -I/usr/local/include -I../runtime -L/usr/lib -L/usr/local/lib -o ./stats.profile.custom_oz.txt $(BUILD_FULL_DIR)/$(TARGET) -x c $(STATS_DIR)/stats.profile.custom.c_ -lc
	$(CC) $(C_MODE) -Oz -g3 -DDEBUG -static -I/usr/include -I/usr/local/include -I../runtime -L/usr/lib -L/usr/local/lib -o ./stats.profile.std_oz.txt $(BUILD_FULL_DIR)/$(TARGET) -x c $(STATS_DIR)/stats.profile.std.c_ -lc
	$(CC) $(C_MODE) -O2 -g3 -DDEBUG -static -I/usr/include -I/usr/local/include -I../runtime -L/usr/lib -L/usr/local/lib -o ./stats.profile.custom_o3.txt $(BUILD_FULL_DIR)/$(TARGET) -x c $(STATS_DIR)/stats.profile.custom.c_ -lc
	$(CC) $(C_MODE) -O3 -g3 -DDEBUG -static -I/usr/include -I/usr/local/include -I../runtime -L/usr/lib -L/usr/local/lib -o ./stats.profile.std_o3.txt $(BUILD_FULL_DIR)/$(TARGET) -x c $(STATS_DIR)/stats.profile.std.c_ -lc
	valgrind --tool=callgrind --cache-sim=yes --branch-sim=yes $(STATS_DIR)/stats.profile.custom_oz.txt;
	valgrind --tool=callgrind --cache-sim=yes --branch-sim=yes $(STATS_DIR)/stats.profile.std_oz.txt;
	valgrind --tool=callgrind --cache-sim=yes --branch-sim=yes $(STATS_DIR)/stats.profile.custom_o3.txt;
	valgrind --tool=callgrind --cache-sim=yes --branch-sim=yes $(STATS_DIR)/stats.profile.std_o3.txt;

stats: cloc
	echo TRUE

.PHONY : debug
debug:
	scan-build35 --use-cc clang -analyze-headers -o $(STATS_DIR) make 2>&1 | tail -n 1 | sed "s/'scan-view [^']*'/'scan-view &'/"

